=========

You only need to download and run git-playbook, it will download roles and install necessary collections, 
also it will run second playbook inside the downloaded git directory (ansible-task2) and after its done it will display the results
but you need to manually change inventory file (define hosts) also grafana and prometheus vars file which includes ip addreses of hosts
plus grafana vars file includes api keys which should be generated manually and and inserted as variable

Requirements
------------

requirements for roles will be installed automatically when git-playbook runs but if you want to run playbook.yml directly 
you'll have to run "ansible-galaxy install -r requirements.yml"